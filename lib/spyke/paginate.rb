require 'active_support/core_ext'

module Spyke::Paginate
  HEADERS = {
    total_count:  'X-Total',
    total_pages:  'X-Total-Pages',
    limit_value:  'X-Per-Page',
    current_page: 'X-Page',
    next_page:    'X-Next-Page',
    prev_page:    'X-Prev-Page',
    offset_value: 'X-Offset',
    links:        'Link'
  }

  LAMBDAS = {
    total_count: ->(env) { return nil },
    total_pages: ->(env) { return nil },
    limit_value: ->(env) { return nil },
    current_page: ->(env) { return nil },
    next_page: ->(env) { 
                  current= env.response_headers[Spyke::Paginate::HEADERS[:current_page]]&.to_i
                  current.is_a?(Integer) ? current+1 : nil 
                },
    prev_page: ->(env) { 
                   current= env.response_headers[Spyke::Paginate::HEADERS[:current_page]]&.to_i
                   current.is_a?(Integer) && current > 1 ? current+1 : nil 
                 },
    offset_value: ->(env) { return 0 },
    links: ->(env) { return nil }
  }


  METADATA_KEY = :paginate
end

require 'spyke/paginate/header_parser'
require 'spyke/paginate/relation'
require 'spyke/paginate/scopes'
