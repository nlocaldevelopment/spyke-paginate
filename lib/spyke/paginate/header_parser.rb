module Spyke::Paginate
  class HeaderParser < Faraday::Response::Middleware
    def on_complete(env)
      @env = env

      metadata = Spyke::Paginate::HEADERS.each_with_object({}) do |(symbol, key), hash|
        hash[symbol] = header(key) || header(key.downcase) || Spyke::Paginate::LAMBDAS[symbol].call(env)
      end

      @env.body={} if @env.body.blank?
      @env.body[:metadata] ||= {}
      @env.body[:metadata][Spyke::Paginate::METADATA_KEY] = metadata
    end

    private

    def header(key)
      value = @env.response_headers[key]
      value.try(:to_i)
    end
  end
end
