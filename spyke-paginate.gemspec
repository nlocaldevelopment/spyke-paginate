Gem::Specification.new('spyke-paginate', '0.0.5') do |spec|
  spec.authors  = ['Daniel Prado']
  spec.email    = ['daniel.prado@publicar.com']
  spec.homepage = 'https://bitbucket.com/nlocal/spyke-paginate'
  spec.summary  = 'Paginate for Spyke models Kaminari style'
  spec.files    =  Dir['lib/**/*']

  spec.add_runtime_dependency 'spyke', '>=  5.3'
  spec.add_runtime_dependency 'activesupport', '>= 6'

  spec.add_development_dependency 'rspec', '>= 3'
  spec.add_development_dependency 'pry', '0.10.1'
  spec.add_development_dependency 'webmock', '~> 1.20'
end
