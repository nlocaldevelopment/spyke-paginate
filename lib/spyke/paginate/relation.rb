module Spyke::Paginate
  # A Spyke relation extended with pagination metadata and helpers, similar
  # Paginate's PaginatableArray.
  class Relation < Spyke::Relation
    Spyke::Paginate::HEADERS.keys.each do |symbol|
      define_method(symbol) do
        find_some.metadata[Spyke::Paginate::METADATA_KEY][symbol]
      end
    end

    def first_page?
      current_page == 1
    end

    def reload 
      @find_some= nil 
      @find_one= nil
      self
    end 

    def last_page?
      if total_pages 
        current_page == total_pages 
      else 
        self.clone.page(current_page+1).blank?
      end
    end

    def out_of_range?
      if total_pages 
        current_page > total_pages || current_page < 1
      else 
        self.blank? 
      end 
    end

    def next
      self.clone.page(current_page+1).reload
    end 

    def prev
      current_page > 1 ? self.clone.page(current_page-1).reload : nil
    end 

    # Iterates through each page.
    #
    # Useful for stitching together an array of all records, across every page.
    # For example:
    #
    #   users = User.all.each_page.flat_map(&:to_a)
    #
    # If there are 3 total pages, it would result in these HTTP requests:
    #
    #   GET /v1/users
    #   GET /v1/users?page=2
    #   GET /v1/users?page=3
    #
    # ...and this array of users (25 records per page):
    #
    #   users.count
    #   # => 75
    #
    # With the exception of the first one, pages are lazily fetched.
    def each_page
      return to_enum(:each_page) unless block_given?
      
      prev= self.prev
      page= self.clone

      until page.out_of_range? || page.current_page == prev&.current_page
        yield page
        prev= page
        page= page.next
      end
      page
    end

  end
end
